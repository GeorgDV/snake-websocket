import { ISnake } from '../types';

export const boxSize = 15;

export const drawField = (fieldSizeX: number, fieldSizeY: number): void => {
  const $game = document.getElementById('game');
  if (!$game) {
    return;
  }
  $game.innerHTML = '';

  for (let x = 0; x < fieldSizeX; x++) {
    for (let y = 0; y < fieldSizeY; y++) {
      const $box = document.createElement('div');
      $box.setAttribute('id', `box_x${x}_y${y}`);
      $box.style.top = `${x * boxSize}px`;
      $box.style.left = `${y * boxSize}px`;
      $box.style.width = `${boxSize}px`;
      $box.style.height = `${boxSize}px`;

      $box.classList.add('box');
      $game.appendChild($box);
    }
  }
};

export const drawSnakes = (data: { snakes: [ISnake] }): void => {
  const { snakes } = data;
  document.querySelectorAll<HTMLDivElement>('#game .snake').forEach((element) => {
    element.classList.remove('snake');
    element.style.backgroundColor = '';
  });

  snakes.forEach((snake) => {
    snake.coordinates.forEach((c) => {
      const $box = document.getElementById(`box_x${c.x}_y${c.y}`);
      if (!$box) {
        return;
      }
      $box.classList.add('snake');
      $box.style.backgroundColor = snake.color;
    });
  });
};
